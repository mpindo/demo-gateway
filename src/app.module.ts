import { Module } from '@nestjs/common';
import { PostsController } from './posts/posts.controller';
import { TodosController } from './todo/todos.controller';

@Module({
  imports: [],
  controllers: [TodosController, PostsController],
  providers: [],
})
export class AppModule {}
