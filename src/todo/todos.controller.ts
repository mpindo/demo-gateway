import {Body, Controller, Get, Post, Param} from '@nestjs/common';
import {Client, ClientKafka, Transport} from "@nestjs/microservices";
import {ITodo} from "./interfaces/todo.interface";

@Controller('todos')
export class TodosController {
    @Client({
        transport: Transport.KAFKA,
        options: {
            client: {
                clientId: 'todos',
                brokers: ['167.172.71.139:9092','167.172.71.139:9093','167.172.71.139:9094'],
            },
            consumer: {
                groupId: 'demo-consumer'
            }
        }
    })
    client: ClientKafka;

    async onModuleInit() {
        this.client.subscribeToResponseOf('add.new.todo');
        this.client.subscribeToResponseOf('get.todo.list');
        this.client.subscribeToResponseOf('get.todo.id');


        await this.client.connect();
    }

    @Post('/')
    appPost(@Body() todo: ITodo) {
        console.log(todo)
        return this.client.send('add.new.todo', todo);
    }

    @Get('/')
    getList() {
        return this.client.send('get.todo.list', '');
    }

    @Get(':id')
    getid(@Param('id') id: any) {
        return this.client.send('get.todo.id', id);
    }
 }
