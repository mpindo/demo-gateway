import {Body, Controller, Get, Post} from '@nestjs/common';
import {Client, ClientKafka, Transport} from "@nestjs/microservices";
import {IPost} from "./interfaces/post.interface";

@Controller('posts')
export class PostsController {
    @Client({
        transport: Transport.KAFKA,
        options: {
            client: {
                clientId: 'posts',
                brokers: ['167.172.71.139:9092','167.172.71.139:9093','167.172.71.139:9094'],
            },
            consumer: {
                groupId: 'local-post-consumer'
            }
        }
    })
    client: ClientKafka;

    async onModuleInit() {
        this.client.subscribeToResponseOf('add.new.post'); /// add to topic
        this.client.subscribeToResponseOf('get.post.list'); /// add to topic
        this.client.subscribeToResponseOf('get.post.id');
        await this.client.connect();
    }

    @Post('/')
    appPost(@Body() post: IPost) {
        console.log(post)
        return this.client.send('add.new.post', post);
    }

    @Get('/')
    getList() {
        return this.client.send('get.post.list', '');
    }

    // @Get(':id')
    // getid(@Param('id') id: any) {
    //     return this.client.send('get.post.id', id);
    // }
}
